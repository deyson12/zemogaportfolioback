package com.portafolio.portafolio.resource;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.portafolio.portafolio.model.Portfolio;
import com.portafolio.portafolio.repository.IPortfolioRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(PortfolioResource.class)
public class PortfolioResourceTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private IPortfolioRepository portfolioRepository;

	/**
	 * Test for get all portfolios
	 * 
	 * @throws Exception
	 */
	@Test
	public void getListAllPortfolios() throws Exception {

		Portfolio portfolio = new Portfolio();
		portfolio.setIdportfolio(1);
		portfolio.setDescription("Description");
		portfolio.setImageUrl("URL_IMAGE");
		portfolio.setTwitterUserName("DeysonEstrada");
		portfolio.setTitle("Deyson Estrada");

		List<Portfolio> allPortfolios = Arrays.asList(portfolio);

		Mockito.when(portfolioRepository.findAll()).thenReturn(allPortfolios);

		mvc.perform(get("/v1/portfolio").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].description", is(portfolio.getDescription())))
				.andExpect(jsonPath("$[0].imageUrl", is(portfolio.getImageUrl())))
				.andExpect(jsonPath("$[0].twitterUserName", is(portfolio.getTwitterUserName())))
				.andExpect(jsonPath("$[0].title", is(portfolio.getTitle())));
	}

	@Test
	public void getPortfolioById() throws Exception {

		Portfolio portfolio = new Portfolio();
		portfolio.setIdportfolio(1);
		portfolio.setDescription("Description");
		portfolio.setImageUrl("URL_IMAGE");
		portfolio.setTwitterUserName("DeysonEstrada");
		portfolio.setTitle("Deyson Estrada");

		Optional<Portfolio> portfolioReturn = Optional.of(portfolio);

		Mockito.when(portfolioRepository.findById(1)).thenReturn(portfolioReturn);

		mvc.perform(get("/v1/portfolio/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.description", is(portfolio.getDescription())))
				.andExpect(jsonPath("$.imageUrl", is(portfolio.getImageUrl())))
				.andExpect(jsonPath("$.twitterUserName", is(portfolio.getTwitterUserName())))
				.andExpect(jsonPath("$.title", is(portfolio.getTitle())));

	}

	@Test
	public void putPortfolio() throws Exception {

		Portfolio portfolio = new Portfolio();
		portfolio.setIdportfolio(1);
		portfolio.setDescription("Description");
		portfolio.setImageUrl("URL_IMAGE");
		portfolio.setTwitterUserName("DeysonEstrada");
		portfolio.setTitle("Deyson Estrada");

		String body = "{\"idportfolio\": 1,\"description\": \"Description\",\"imageUrl\": \"URL_IMAGE\",\"twitterUserName\": \"DeysonEstrada\",\"title\": \"Deyson Estrada\"}";

		Mockito.when(portfolioRepository.save(any())).thenReturn(portfolio);

		mvc.perform(put("/v1/portfolio/1").contentType(MediaType.APPLICATION_JSON).content(body))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.code", is("OK")))
				.andExpect(jsonPath("$.data.description", is(portfolio.getDescription())));

	}

	@Test
	public void deletePortfolio() throws Exception {

		Mockito.doNothing().when(portfolioRepository).deleteById(1);

		mvc.perform(delete("/v1/portfolio/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.code", is("OK")));

	}

}
