package com.portafolio.portafolio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author deyson.estrada
 *
 */
@Entity
public class Portfolio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idportfolio;
	private String description;
	@Column(name = "image_url")
	private String imageUrl;
	@Column(name = "twitter_user_name")
	private String twitterUserName;
	private String title;	
	
	public Portfolio() {
	}

	public Integer getIdportfolio() {
		return idportfolio;
	}

	public void setIdportfolio(Integer idportfolio) {
		this.idportfolio = idportfolio;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTwitterUserName() {
		return twitterUserName;
	}

	public void setTwitterUserName(String twitterUserName) {
		this.twitterUserName = twitterUserName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
