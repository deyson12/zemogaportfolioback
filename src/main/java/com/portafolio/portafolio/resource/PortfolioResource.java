package com.portafolio.portafolio.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.portafolio.model.Portfolio;
import com.portafolio.portafolio.model.Response;
import com.portafolio.portafolio.repository.IPortfolioRepository;

/**
 * 
 * @author deyson.estrada
 *
 */
@RestController
@RequestMapping("/v1/portfolio")
public class PortfolioResource {

	@Autowired
	IPortfolioRepository portfolioRepository;

	/**
	 * Get All portfolios
	 * @return
	 */
	@GetMapping
	public List<Portfolio> getAll() {
		return portfolioRepository.findAll();
	}

	/**
	 * Get portfolio by id
	 * @param idportfolio
	 * @return
	 */
	@GetMapping("/{idportfolio}")
	public Optional<Portfolio> getPortfolioById(@PathVariable("idportfolio") int idportfolio) {
		return portfolioRepository.findById(idportfolio);
	}

	/**
	 * Update all data of portfolio
	 * @param idportfolio
	 * @param portfolio
	 * @return
	 */
	@PutMapping("/{idportfolio}")
	public Response putPortfolio(@PathVariable("idportfolio") int idportfolio, @RequestBody Portfolio portfolio) {
		Response response = new Response();
		try {
			portfolio.setIdportfolio(idportfolio);
			response.setCode("OK");
			response.setData(portfolioRepository.save(portfolio));
		} catch (Exception e) {
			response.setCode("ERR");
			response.setData("Error deleting the portfolio");
		}
		return response;
	}
	
	/**
	 * Delete portfolio by id
	 * @param idportfolio
	 * @return
	 */
	@DeleteMapping("/{idportfolio}")
	public Response update(@PathVariable("idportfolio") int idportfolio) {
		Response response = new Response();
		try {
			portfolioRepository.deleteById(idportfolio);
			response.setCode("OK");
			response.setData("Portfolio deleted successful");
		}catch (Exception e) {
			response.setCode("ERR");
			response.setData("Portfolio does not exist");
		}
		return response;
	}

}
