package com.portafolio.portafolio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.portafolio.portafolio.model.Portfolio;

/**
 * 
 * @author deyson.estrada
 *
 */
public interface IPortfolioRepository extends JpaRepository<Portfolio, Integer>{

}
