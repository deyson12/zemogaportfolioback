<div align="center">
  <a href="http://deysonestrada.com"><img width="100%" src="https://raw.githubusercontent.com/deyson12/deyson12/master/github.png" alt="cover" /></a>
</div>

## Hi, I'm Deyson Estrada, a Developer 🧑🏽‍💻 from Colombia. I ❤️ code

<h2><i>Steps to install Zemoga Portfolio by Deyson:</i></h2>

<ul>
    <li>1. Clone the project: (git clone https://gitlab.com/deyson12/zemogaportfolioback.git)</li>
    <li>2. Import the project with Maven</li>
    <li>3. Validate that the JRE is Java SE 14 if not change it</li>
</ul>

<h2><i>Methods in the Portfolio REST API:</i></h2>

<h3>With a REST Client like (Insomnia, Postman, SOAPUI, etc) and Create a request</h3>

<ul>
    <li>1. GET http://localhost:8080/v1/portfolio (Find All Portfolios)</li>
    <li>2. GET http://localhost:8080/v1/portfolio/XX (Find Portfolio by ID where XX is the portfolio's id)</li>
    <li>3. PUT http://localhost:8080/v1/portfolio/XX (Update Portfolio by ID where XX is the portfolio's id, you need to send the information in the body)</li>
    <li>4. DELETE http://localhost:8080/v1/portfolio/XX (Delete Portfolio by ID where XX is the portfolio's id)</li>
</ul>

<h3>Portfolio example body for UPDATE</h3>

{
  "description": "Description",
  "imageUrl": "https://cdn.dribbble.com/users/5691030/avatars/normal/c3c35b90da6b6106b6eddffa12da482e.png?1597150297",
  "twitterUserName": "DeysonEstrada",
  "title": "Deyson Estrada"
}

<h2><i>Follow me:</i></h2>
<div>
  <a href="https://www.linkedin.com/in/dfep/" target="_blank">
    <img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white&color=071A2C" alt="LinkedIn">
  </a>
  <a href="https://www.instagram.com/deysonestrad/" target="_blank">
    <img src="https://img.shields.io/badge/Instagram-%23E4405F.svg?&style=flat-square&logo=instagram&logoColor=white&color=071A2C" alt="Instagram">
  </a>
   <a href="mailto:deyson12@gmail.com" mailto="deyson12@gmail.com" target="_blank">
    <img src="https://img.shields.io/badge/Gmail-%231877F2.svg?&style=flat-square&logo=gmail&logoColor=white&color=071A2C" alt="Gmail">
  </a>
  <a href="http://deysonestrada.com" target="_blank">
    <img src="https://img.shields.io/badge/Freelancer-%231877F2.svg?&style=flat-square&logo=freelancer&logoColor=white&color=071A2C" alt="Freelancer">
  </a>
</div>
<br />

🧑🏽‍💻 I’m currently working at [Sura](https://www.suramericana.com/index.html)

☕ • Back-End: Java <br>
🅰️ • Front End: Angular <br>
💽 • DataBases: Oracle, Mysql, MongoDB <br>
✒️ • Design: Figma and Adobe XD <br>
🗣️ • English: B1 <br>

[deysonestrada.com](http://deysonestrada.com)
